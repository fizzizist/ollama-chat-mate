#!/bin/bash

echo "updating files"
for i in setup.py README.md gpt_chat_mate/__init__.py; do
	sed -i "s/$1/$2/g" $i
done

echo "running git ops"
git commit -am "Bumping version from v$1 to v$2"
git tag -a v$2 -m "$3"
git push
git push origin v$2
