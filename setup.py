from setuptools import find_packages, setup

with open("README.md") as f:
    ld = f.read()

setup(
    name="ollama-chat-mate",
    version="1.10.3",
    description="A cli app for communicating with ollama models",
    license="GPLv3",
    url="https://gitlab.com/fizzizist/ollama-chat-mate",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "ollama-chat-mate=ollama_chat_mate.main:main",
        ],
    },
    install_requires=[
        "pygments",
        "ollama",
        "pyfiglet",
        "func-timeout",
        "tiktoken",
    ],
    author="Fizzizist",
    author_email="ollama-chat-mate.unadorned941@passmail.net",
    long_description=ld,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
